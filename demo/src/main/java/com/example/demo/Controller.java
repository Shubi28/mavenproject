package com.example.demo;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Controller {
    @Autowired
    StudentDAO ob;
@GetMapping("/hello")
public String display() {
    return "Welcome to Microservices!";
}
@GetMapping("/hello1")
public StudentBean display1() {
    return new StudentBean("Sudarshan",21,"Gurgaon");
}
@GetMapping("/student")
public List<StudentBean> getStudents(){
    return ob.getStudents();
}
@GetMapping("/student/{name}")
public List<StudentBean> getStudents2(@PathVariable String name){
    return ob.getDetails(name);
}
@PostMapping("/student")
public String insert(@RequestBody StudentBean bean) {
    return ob.insert(bean);
}
@DeleteMapping("/student")
public String insert(@RequestBody String name) {
    return ob.delete(name);
}
@PutMapping("/student/{name}")
public String insert(@RequestBody StudentBean bean,@PathVariable String name) {
    return ob.update(name,bean);
}
}