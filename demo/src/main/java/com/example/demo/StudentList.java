package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service //Object is automatically created
public class StudentList {
	
	private List<StudentBean> list;

	public StudentList() {
		super();
		list=new ArrayList<>();
		list.add(new StudentBean("Shubi",23,"Patiala"));
		list.add(new StudentBean("Kanu",22,"Jalandhar"));
		list.add(new StudentBean("Balbeer",23,"Bhopal"));
		list.add(new StudentBean("Siddhant",21,"Jammu"));
	}

	public List<StudentBean> getList() {
		return list;
	}

	public void setList(List<StudentBean> list) {
		this.list = list;
	}

}
