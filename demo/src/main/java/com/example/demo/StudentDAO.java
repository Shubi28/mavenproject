package com.example.demo;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentDAO {
	
	@Autowired
	StudentList l1;
	public List<StudentBean> getStudents(){
		return l1.getList();
	}
	
	
	public List<StudentBean> getDetails(String name) {
		return l1.getList().stream().filter(e->e.getName().equals(name)).collect(Collectors.toList());
	}
	
	public String insert(StudentBean ob) {
		l1.getList().add(ob);
		return "Added Successfully";
	}
	
	public String delete(String name) {
		l1.getList().removeIf(e->e.getName().equals(name));
		return "Removed Successfully";
	}
	
	public String update(String name,StudentBean ob) {
		String name1=ob.getName();
		if(name.equals(name1)) {
			l1.getList().removeIf(e->e.getName().equals(name));
			l1.getList().add(ob);
		}else {
			return "No Such Person found in the list ";
		}
		return "Updated successfully";
	}
}
