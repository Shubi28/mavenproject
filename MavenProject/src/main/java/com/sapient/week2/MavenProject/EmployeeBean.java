package com.sapient.week2.MavenProject;

public class EmployeeBean {

	private int ID;
	private String Name;
	private int Salary;
	
	public EmployeeBean() {
		super();
	}
	
	public EmployeeBean(String Name,int ID,int Salary) {
		super();
		this.Name=Name;
		this.ID=ID;
		this.Salary=Salary;
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		ID = ID;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		Name = Name;
	}

	public int getSalary() {
		return Salary;
	}
	
	public void setSalary(int Salary) {
		Salary = Salary;
	}

	@Override
	public String toString() {
		return "EmployeeBean [ID=" + ID + ", Name=" + Name + ", Salary=" + Salary + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeBean other = (EmployeeBean) obj;
		if (ID != other.ID)
			return false;
		if (Name == null) {
			if (other.Name != null)
				return false;
		} else if (!Name.equals(other.Name))
			return false;
		if (Salary != other.Salary)
			return false;
		return true;
	}
	
	

}
